package demo.niladree.myweatherforecast.features.weather;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import demo.niladree.myweatherforecast.R;
import demo.niladree.myweatherforecast.dataTypes.forecastResponse.DataList;

/**
 * Created by niladree on 17/03/2017.
 */

public class ForecastRecyclerViewLayoutAdapter extends RecyclerView.Adapter<ForecastRecyclerViewLayoutAdapter.ForecastHolderView>{

    private Context context;
    private ArrayList<DataList> weatherData;

    //CONSTRUCTOR
    public ForecastRecyclerViewLayoutAdapter(Context context){
        this.context = context;
    }

    @Override
    public ForecastHolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
        return new ForecastHolderView(view);
    }

    @Override
    public void onBindViewHolder(final ForecastHolderView holder, final int position) {
        DateTime dateTime = new DateTime(weatherData.get(holder.getAdapterPosition()).getDt()*1000);
        if(dateTime.dayOfWeek().equals(new DateTime().dayOfWeek()))
            holder.dayTV.setText(context.getString(R.string.today));
        else
            holder.dayTV.setText(dateTime.dayOfWeek().getAsText());
        String iconUrl = String.format(context.getString(R.string.iconUrl), weatherData.get(holder.getAdapterPosition()).getWeather().get(0).getIcon());
        Picasso.with(context).load(iconUrl).fit().centerCrop().into(holder.weatherIV);
        holder.maxTempTV.setText(String.format(context.getString(R.string.temperature),new DecimalFormat("#").format(weatherData.get(holder.getAdapterPosition()).getMain().getTempMax())));
        holder.minTempTV.setText(String.format(context.getString(R.string.temperature),new DecimalFormat("#").format(weatherData.get(holder.getAdapterPosition()).getMain().getTempMin())));
    }

    @Override
    public int getItemCount() {
        if(weatherData != null)
            return weatherData.size();
        else
            return 0;
    }

    public void loadWeather(ArrayList<DataList> weatherData) {
        this.weatherData = weatherData;
    }

    static class ForecastHolderView extends RecyclerView.ViewHolder{

        @BindView(R.id.dayTV) AppCompatTextView dayTV;
        @BindView(R.id.weatherIV) AppCompatImageView weatherIV;
        @BindView(R.id.maxTempTV) AppCompatTextView maxTempTV;
        @BindView(R.id.minTempTV) AppCompatTextView minTempTV;

        public ForecastHolderView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
