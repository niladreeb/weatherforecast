package demo.niladree.myweatherforecast.features.weather;

/**
 * Created by niladree on 17/03/2017.
 */

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import demo.niladree.myweatherforecast.shared.globalInterface.DataInterface;

/**
 * Provides data for {@link WeatherLogicInterface}
 */
public interface WeatherDataInterface extends DataInterface {

    /**
     * GET 5 day forecast
     * @param city query city
     * @param unit unit system of temperature
     * @param appId open weather api key
     * @param callback callback to the calling class
     */
    void getFiveDayForecast(String city, String unit, String appId, Callback<Forecast> callback);
}
