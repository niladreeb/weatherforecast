package demo.niladree.myweatherforecast.features.weather;

/**
 * Created by niladree on 17/03/2017.
 */

/**
 * Provides logic for {@link WeatherViewInterface}
 */
public interface WeatherLogicInterface {

    void viewReady(String city);

}
