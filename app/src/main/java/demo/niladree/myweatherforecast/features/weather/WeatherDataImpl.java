package demo.niladree.myweatherforecast.features.weather;

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import demo.niladree.myweatherforecast.shared.api.ApiFailure;
import demo.niladree.myweatherforecast.shared.api.ApiService;
import demo.niladree.myweatherforecast.shared.globalInterface.DataInterface;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by niladree on 17/03/2017.
 */

public class WeatherDataImpl implements WeatherDataInterface {

    private ApiService apiService;
    private ApiFailure apiFailure;

    //CONSTRUCTOR
    public WeatherDataImpl(ApiService apiService, ApiFailure apiFailure){
        this.apiService = apiService;
        this.apiFailure = apiFailure;
    }

    //WeatherDataInterface Methods
    @Override
    public void getFiveDayForecast(String city, String unit, final String appId, final DataInterface.Callback<Forecast> callback) {
        apiService.getFiveDayForecast(city, unit, appId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Forecast>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        apiFailure.onApiFailure(callback, e);
                    }

                    @Override
                    public void onNext(Forecast forecast) {
                        callback.onResponse(forecast);
                    }
                });
    }
}
