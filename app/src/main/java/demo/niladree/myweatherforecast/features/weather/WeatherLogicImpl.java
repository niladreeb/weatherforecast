package demo.niladree.myweatherforecast.features.weather;

import android.support.annotation.NonNull;

import demo.niladree.myweatherforecast.BuildConfig;
import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import demo.niladree.myweatherforecast.shared.MessageFactory;
import demo.niladree.myweatherforecast.shared.globalInterface.DataInterface;
import demo.niladree.myweatherforecast.shared.storage.ForecastStorageInterface;

/**
 * Created by niladree on 17/03/2017.
 */

public class WeatherLogicImpl implements WeatherLogicInterface {

    private WeatherViewInterface view;
    private WeatherDataInterface data;
    private MessageFactory messageFactory;
    private ForecastStorageInterface forecastStorage;
    private final String UNIT = "metric";

    //CONSTRUCTOR
    public WeatherLogicImpl(WeatherViewInterface view, WeatherDataInterface data, MessageFactory messageFactory, ForecastStorageInterface forecastStorage){
        this.view = view;
        this.data = data;
        this.messageFactory = messageFactory;
        this.forecastStorage = forecastStorage;
    }

    //WeatherLogicInterface Methods
    @Override
    public void viewReady(String city) {
        view.showProgressBar();
        data.getFiveDayForecast(city, UNIT, BuildConfig.WEATHERKEY, new DataInterface.Callback<Forecast>() {
            @Override
            public void onResponse(@NonNull Forecast forecast) {
                if(forecast.getCod().equalsIgnoreCase("200")) {
                    view.showForecast(forecast);
                    forecastStorage.saveWeatherData(forecast);
                }
                else {
                    if(forecastStorage.getWeatherData() != null)
                        view.showForecast(forecastStorage.getWeatherData());
                    else
                        view.displayMessage(messageFactory.invalidInput());
                }
            }

            @Override
            public void onFailureWithMessage(String message) {
                if(forecastStorage.getWeatherData() != null)
                    view.showForecast(forecastStorage.getWeatherData());
                else
                    view.displayMessage(message);
            }

            @Override
            public void onFailure() {
                if(forecastStorage.getWeatherData() != null)
                    view.showForecast(forecastStorage.getWeatherData());
                else
                    view.displayMessage(messageFactory.somethingWentWrongError());
            }

            @Override
            public void onNoNetworkFailure() {
                if(forecastStorage.getWeatherData() != null)
                    view.showForecast(forecastStorage.getWeatherData());
                else
                    view.displayMessage(messageFactory.networkError());
            }
        });
    }
}
