package demo.niladree.myweatherforecast.features.weather;

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import demo.niladree.myweatherforecast.shared.globalInterface.ViewInterface;

/**
 * Created by niladree on 17/03/2017.
 */

public interface WeatherViewInterface extends ViewInterface {

    void showForecast(Forecast forecast);
}
