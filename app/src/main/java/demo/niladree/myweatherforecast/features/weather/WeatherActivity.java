package demo.niladree.myweatherforecast.features.weather;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import demo.niladree.myweatherforecast.R;
import demo.niladree.myweatherforecast.dataTypes.forecastResponse.DataList;
import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import demo.niladree.myweatherforecast.features.location.LocationActivity;
import demo.niladree.myweatherforecast.shared.WeatherForecastApplication;

public class WeatherActivity extends AppCompatActivity implements WeatherViewInterface{

    @Inject WeatherLogicInterface logic;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.rootView) ViewGroup rootView;
    @BindView(R.id.cityTV) AppCompatTextView cityTV;
    @BindView(R.id.dateTV) AppCompatTextView dateTV;
    @BindView(R.id.iconIV) AppCompatImageView iconIV;
    @BindView(R.id.temperatureTV) AppCompatTextView temperatureTV;
    @BindView(R.id.descriptionTV) AppCompatTextView descriptionTV;
    @BindView(R.id.forecastRV) RecyclerView forecastRV;
    @BindView(R.id.progressBarLayout) ViewGroup progressBarLayout;

    private ForecastRecyclerViewLayoutAdapter forecastRecyclerViewLayoutAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependencies();
        setupUI();
    }

    private void initDependencies() {
        ((WeatherForecastApplication)getApplication()).getObjectGraph().plus(new WeatherDIModule(this)).inject(this);
    }

    private void setupUI() {
        setContentView(R.layout.activity_weather);
        ButterKnife.bind(this);
        initToolbar();
        setupRecyclerView();
        logic.viewReady("London");
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
            actionBar.setTitle("");
    }

    private void setupRecyclerView() {
        forecastRecyclerViewLayoutAdapter = new ForecastRecyclerViewLayoutAdapter(this);
        forecastRV.setAdapter(forecastRecyclerViewLayoutAdapter);
        forecastRV.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cities, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cities:
                startActivity(new Intent(this, LocationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //INTERFACE METHODS
    //WeatherViewInterface Methods
    @Override
    public void showForecast(Forecast forecast) {
        hideProgressBar();
        if(!forecast.getList().isEmpty()) {
            if(!forecast.getList().get(0).getWeather().isEmpty()) {
                cityTV.setText(String.format(getString(R.string.place), forecast.getCity().getName(), forecast.getCity().getCountry()));
                DateTime dateTime = new DateTime(forecast.getList().get(0).getDt()*1000);
                dateTV.setText(dateTime.toString(DateTimeFormat.forPattern("EE, dd MMMM HH:mm")));
                temperatureTV.setText(String.format(getString(R.string.temperature), new DecimalFormat("#").format(forecast.getList().get(0).getMain().getTemp())));
                descriptionTV.setText(forecast.getList().get(0).getWeather().get(0).getDescription());
                String iconUrl = String.format(getString(R.string.iconUrl), forecast.getList().get(0).getWeather().get(0).getIcon());
                Picasso.with(this).load(iconUrl).fit().centerCrop().into(iconIV);
            }
            ArrayList<DataList> filteredData = new ArrayList<>();
            DateTime dateTime;
            String day = "";
            for(int i=0; i<forecast.getList().size(); i++){
                dateTime = new DateTime(forecast.getList().get(i).getDt()*1000);
                if(day.equalsIgnoreCase("")) {
                    filteredData.add(forecast.getList().get(i));
                    day = dateTime.dayOfMonth().getAsString();
                }
                else if(!day.equalsIgnoreCase(dateTime.dayOfMonth().getAsString())){
                    filteredData.add(forecast.getList().get(i));
                    day = dateTime.dayOfMonth().getAsString();
                }
            }
            forecastRecyclerViewLayoutAdapter.loadWeather(filteredData);
            forecastRecyclerViewLayoutAdapter.notifyDataSetChanged();
        }

    }

    //ViewInterface Methods
    @Override
    public void displayMessage(String message) {
        hideProgressBar();
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.INVISIBLE);
    }
}
