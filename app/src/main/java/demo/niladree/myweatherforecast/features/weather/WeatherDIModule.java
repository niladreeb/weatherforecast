package demo.niladree.myweatherforecast.features.weather;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import demo.niladree.myweatherforecast.shared.MessageFactory;
import demo.niladree.myweatherforecast.shared.api.ApiFailure;
import demo.niladree.myweatherforecast.shared.api.ApiService;
import demo.niladree.myweatherforecast.shared.storage.ForecastStorageInterface;

/**
 * Created by niladree on 17/03/2017.
 */

@Module(injects = WeatherActivity.class, complete = false)
public class WeatherDIModule {

    private WeatherViewInterface view;

    //CONSTRUCTOR
    public WeatherDIModule(WeatherViewInterface view){
        this.view = view;
    }

    @Provides @Singleton
    public WeatherDataInterface provideWeatherDataInterface(ApiService apiService, ApiFailure apiFailure){
        return new WeatherDataImpl(apiService, apiFailure);
    }

    @Provides @Singleton
    public WeatherLogicInterface provideWeatherLogicInterface(WeatherDataInterface data, MessageFactory messageFactory, ForecastStorageInterface forecastStorage){
        return new WeatherLogicImpl(view, data, messageFactory, forecastStorage);
    }
}
