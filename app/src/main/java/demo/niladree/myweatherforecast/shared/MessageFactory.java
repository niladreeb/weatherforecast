package demo.niladree.myweatherforecast.shared;

import android.content.Context;
import demo.niladree.myweatherforecast.R;

/**
 * Created by niladree on 17/03/2017.
 */

public class MessageFactory {

    private Context ctx;

    //CONSTRUCTOR
    public MessageFactory(Context ctx){
        this.ctx = ctx;
    }

    public String somethingWentWrongError() {return ctx.getString(R.string.somethingWentWrongError);}

    public String networkError() {return ctx.getString(R.string.networkError);}

    public String invalidInput() {return ctx.getString(R.string.invalidCity);}
}
