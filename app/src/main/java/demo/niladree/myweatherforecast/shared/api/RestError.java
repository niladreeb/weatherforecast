package demo.niladree.myweatherforecast.shared.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by niladree on 17/03/2017.
 */

public class RestError {

    @SerializedName("error") private List<String> error;

    //CONSTRUCTOR
    public RestError(List<String> error){
        setError(error);
    }

    //GETTER
    public List<String> getError() {
        return error;
    }

    //SETTER
    public void setError(List<String> error) {
        this.error = error;
    }
}
