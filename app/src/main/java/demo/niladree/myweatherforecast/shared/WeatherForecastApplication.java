package demo.niladree.myweatherforecast.shared;

import android.app.Application;
import java.util.Arrays;
import java.util.List;
import dagger.ObjectGraph;
import demo.niladree.myweatherforecast.shared.di.SharedDIModule;
import demo.niladree.myweatherforecast.shared.di.StorageDIModule;

/**
 * Created by niladree on 17/03/2017.
 */

public class WeatherForecastApplication extends Application{

    private ObjectGraph objectGraph;

    @Override
    public void onCreate(){
        super.onCreate();

        //DAGGER
        objectGraph = ObjectGraph.create(getModules().toArray());
        objectGraph.inject(this);

    }

    private List<Object> getModules(){
        return Arrays.<Object>asList(new StorageDIModule(this), new SharedDIModule(this));
    }

    public ObjectGraph getObjectGraph(){
        return objectGraph;
    }
}
