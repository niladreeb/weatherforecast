package demo.niladree.myweatherforecast.shared.api;

import demo.niladree.myweatherforecast.BuildConfig;
import retrofit.RestAdapter;

/**
 * Created by niladree on 17/03/2017.
 */

public class RetrofitSetup {

    private static final String BASE_URL_CLIENT = BuildConfig.HOST;

    public ApiService getService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL_CLIENT)
                .setLogLevel(getLoggingLevel())
                .build();

        return restAdapter.create(ApiService.class);
    }

    private RestAdapter.LogLevel getLoggingLevel() {

        if (BuildConfig.DEBUG) {
            return RestAdapter.LogLevel.FULL;
        } else {
            return RestAdapter.LogLevel.NONE;
        }
    }
}
