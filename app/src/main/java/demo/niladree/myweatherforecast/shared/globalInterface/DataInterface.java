package demo.niladree.myweatherforecast.shared.globalInterface;

import android.support.annotation.NonNull;

/**
 * Created by niladree on 17/03/2017.
 */

public interface DataInterface {

    public static final String NETWORK_ERROR = "NETWORK";

    interface Callback<T> {

        /** Called when response is received */
        void onResponse(@NonNull T response);

        /**A response from the server with a message*/
        void onFailureWithMessage(String message);

        /** Response has not been received from server */
        void onFailure();

        /** Response has not been received because of network error*/
        void onNoNetworkFailure();
    }

}
