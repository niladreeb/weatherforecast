package demo.niladree.myweatherforecast.shared.di;

import android.content.Context;
import android.content.SharedPreferences;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import demo.niladree.myweatherforecast.shared.WeatherForecastApplication;
import demo.niladree.myweatherforecast.shared.storage.ForecastStorageImpl;
import demo.niladree.myweatherforecast.shared.storage.ForecastStorageInterface;

/**
 * Created by niladree on 17/03/2017.
 */

@Module(injects = StorageDIModule.class, library = true)
public class StorageDIModule {

    private static final String KEY_PERSISTENT_STORAGE = "weather_forecast_persistent_storage";

    private Context ctx;

    //CONSTRUCTOR
    public StorageDIModule(Context ctx) {
        this.ctx = ctx;
    }

    @Provides @Singleton
    public SharedPreferences providesSharedPreferences() {
        SharedPreferences sp = ((WeatherForecastApplication) ctx).getSharedPreferences(KEY_PERSISTENT_STORAGE, Context.MODE_PRIVATE);
        return sp;
    }

    @Provides @Singleton
    public ForecastStorageInterface provideForecastStorageInterface(SharedPreferences sharedPreferences){
        return new ForecastStorageImpl(sharedPreferences);
    }
}
