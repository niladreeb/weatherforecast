package demo.niladree.myweatherforecast.shared.api;

import demo.niladree.myweatherforecast.shared.globalInterface.DataInterface;

/**
 * Created by niladree on 17/03/2017.
 */

public class ApiFailure {

    public void onApiFailure(DataInterface.Callback callback, Throwable e){

        if(e instanceof NullPointerException){
            //DO NOTHING
        }

        else {
            retrofit.RetrofitError rError = (retrofit.RetrofitError) e;

            if (rError.getKind().toString().equals(DataInterface.NETWORK_ERROR)) {
                callback.onNoNetworkFailure();
            } else if (rError.getResponse().getStatus() == 400 || rError.getResponse().getStatus() == 401) {
                //GET THE ERROR BODY HERE AND SEND TO CALLBACK
                try {
                    RestError restError = (RestError) rError.getBodyAs(RestError.class);
                    callback.onFailureWithMessage(restError.getError().get(0));
                } catch (Exception excep) {
                    callback.onFailure();
                }

            } else {
                callback.onFailure();
            }
        }
    }
}
