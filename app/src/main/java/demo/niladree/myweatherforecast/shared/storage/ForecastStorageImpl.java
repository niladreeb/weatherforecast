package demo.niladree.myweatherforecast.shared.storage;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;

/**
 * Created by niladree on 17/03/2017.
 */
public class ForecastStorageImpl implements ForecastStorageInterface {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    //CONSTRUCTOR
    public ForecastStorageImpl(SharedPreferences sharedPreferences){
        this.sharedPreferences = sharedPreferences;
        editor = sharedPreferences.edit();
    }

    @Override
    public void saveWeatherData(Forecast forecast) {
        String forecastString = new Gson().toJson(forecast);
        editor.putString(FORECAST_TOKEN, forecastString);
        editor.apply();

    }

    @Override
    public Forecast getWeatherData() {
        String forecastString = sharedPreferences.getString(FORECAST_TOKEN, null);
        if(forecastString != null){
            return new Gson().fromJson(forecastString, Forecast.class);
        }
        else{
            return null;
        }
    }
}
