package demo.niladree.myweatherforecast.shared.api;

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by niladree on 17/03/2017.
 */

public interface ApiService {

    @GET("/forecast")
    Observable<Forecast> getFiveDayForecast(@Query("q") String city, @Query("units") String unit, @Query("APPID") String appId);

}
