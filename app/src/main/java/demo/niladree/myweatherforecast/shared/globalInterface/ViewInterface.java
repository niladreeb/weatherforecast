package demo.niladree.myweatherforecast.shared.globalInterface;

/**
 * Created by niladree on 17/03/2017.
 */

public interface ViewInterface {

    void displayMessage(String message);

    void showProgressBar();

    void hideProgressBar();
}
