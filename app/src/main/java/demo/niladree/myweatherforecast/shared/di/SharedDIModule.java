package demo.niladree.myweatherforecast.shared.di;

import android.content.Context;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import demo.niladree.myweatherforecast.shared.MessageFactory;
import demo.niladree.myweatherforecast.shared.WeatherForecastApplication;
import demo.niladree.myweatherforecast.shared.api.ApiFailure;
import demo.niladree.myweatherforecast.shared.api.ApiService;
import demo.niladree.myweatherforecast.shared.api.RetrofitSetup;

/**
 * Created by niladree on 17/03/2017.
 */

@Module(injects = WeatherForecastApplication.class, complete = false, library = true)
public class SharedDIModule {

    private Context ctx;

    //CONSTRUCTOR
    public SharedDIModule(Context ctx) {this.ctx = ctx; }

    @Provides @Singleton
    public MessageFactory provideMessageFactory(){
        return new MessageFactory(ctx);
    }

    @Provides @Singleton
    public ApiService provideApiService() {
        RetrofitSetup retrofitSetup = new RetrofitSetup();
        return retrofitSetup.getService();
    }

    @Provides @Singleton
    public ApiFailure provideApiFailure(){
        return new ApiFailure();
    }

}
