package demo.niladree.myweatherforecast.shared.storage;

import demo.niladree.myweatherforecast.dataTypes.forecastResponse.Forecast;

/**
 * Created by niladree on 17/03/2017.
 */

public interface ForecastStorageInterface {

    public static final String FORECAST_TOKEN = "FORECAST_TOKEN";

    void saveWeatherData(Forecast forecast);

    Forecast getWeatherData();
}
