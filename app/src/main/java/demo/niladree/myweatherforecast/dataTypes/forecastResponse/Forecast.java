package demo.niladree.myweatherforecast.dataTypes.forecastResponse;

import java.util.ArrayList;

/**
 * Created by niladree on 17/03/2017.
 */

public class Forecast {

    private String cod;
    private ArrayList<DataList> list;
    private City city;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public ArrayList<DataList> getList() {
        return list;
    }

    public void setList(ArrayList<DataList> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
