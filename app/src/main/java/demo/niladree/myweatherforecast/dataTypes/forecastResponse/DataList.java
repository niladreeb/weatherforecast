package demo.niladree.myweatherforecast.dataTypes.forecastResponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by niladree on 17/03/2017.
 */

public class DataList {

    private long dt;
    private Main main;
    private ArrayList<Weather> weather;
    @SerializedName("dt_txt") private String date;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
