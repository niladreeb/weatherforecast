# This is a demo weather forecast application#

**Content:** The source code and .apk file

** Features **

* Display today's and next five day's weather forecast
* Offline storage functionality.If the app is opened once then later if there is no internet the app will still open and show the last data
* Displays weather for London only in metric system

**Steps for running project in device**

* Download apk from https://drive.google.com/file/d/0B27wNAb63PLpbk42alJrZ1R3am8/view?usp=sharing
* Send the apk to a mobile device and click on the apk
* Device will ask for installing app from unknown sources. Allow for this only.
* Once installed the application is ready to run

**Steps for running project in emulator**

* Clone project to local directory
* Open with Android Studio
* Run on emulator
* Note that the latest version of Android Studio is required 

**Improvements** 

* The cities screen can be developed to display all the cities of the world, which can be filtered as per the user input. Then weather forecast will be dynamic for the selected city.
* Favourite cities can be stored so that it shows the weather of all those places.
* Settings can be added to change temperature units
* Get present location feature to show the temperature of present location.
* Display of more weather data like rain, humidity, wind, etc.
* Graphical representation of weather data and map view
* Better UI and weather icons
* Test cases